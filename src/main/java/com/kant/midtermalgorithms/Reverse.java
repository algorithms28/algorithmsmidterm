
package com.kant.midtermalgorithms;

import java.util.Scanner;


public class Reverse {
    public static void main(String args[]) {
        Scanner kb = new Scanner(System.in);
        int amount; 
        amount = kb.nextInt(); //รับค่าตัวเลขจำนวนเต็ม ที่เป็นจำนวนของข้อมูลตัวเลขจำนวนเต็มที่ต้องใส่ใน Array
        String[] number = new String[amount];////กำหนดจำนวนข้อมูลตัวเลขจำนวนเต็มให้กับ Array
        
        for (int i = 0; i < amount; i++) {////for loop สำหรับการวนรับค่าตัวเลข
            number[i] = kb.next();////รับค่าตัวเลขจำนวนเต็มแล้วเก็บไว้ใน Array
        }
        
        kb.close();
        
        for (int i = number.length - 1; i >= 0; i--) {//สร้าง for loop แล้วทำการย้อนกลับตัวเลขทุกๆตำแหน่งใน Array
            System.out.print(number[i] + " ");//แสดงผลตัวเลขที่อยู่ใน Array ทีละตัว
        }
    }
}
