package com.kant.midtermalgorithms;

import java.util.Scanner;

public class SubArray {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);//
        
        int amount = input.nextInt(); //สร้างตัวแปร amount และรับค่าตัวเลขจำนวนเต็ม ที่เป็นจำนวนของข้อมูลตัวเลขจำนวนเต็มที่ต้องใส่ใน Array
        int arr[] = new int[amount]; //กำหนดจำนวนข้อมูลตัวเลขจำนวนเต็มให้กับ Array
        
        for (int i = 0; i < amount; i++) { //for loop สำหรับการวนรับค่าตัวเลข
            arr[i] = input.nextInt(); 
        }
        
        int max = 1, lenght = 1, maxIndex = 0;
        
        for (int i = 1; i < amount; i++) {
            
            if (arr[i] > arr[i - 1]) { //เช็คว่าข้อมูลตัวเลขจำนวนเต็มมีค่ามากกว่าตัวก่อนหน้านี้หรือไม่
                lenght++; //ถ้ามีค่ามากกว่า ก็ทำการเพิ่มความยาวของ SubArray ที่ยาวที่สุดในขณะนั้น
            } else { //ถ้าไม่
                if (max < lenght) { //เช็คอีกรอบว่าความยาวของSubArray ที่ยาวที่สุด น้อยกว่า ความยาวSubArrayในปัจจุบัน
                    max = lenght; //ถ้าใช้ อัปเดตค่า max
                    maxIndex = i - max; //และอัปเดตค่า maxIndex
                }
                lenght = 1; //reset ค่าlenght ให้เป็น 1 เพื่อไว้เช็คสำหรับ for loop ถัดไป
            }
        }
        
        if (max < lenght) { 
            max = lenght;
            maxIndex = amount - max;
            
        }
        for (int i = maxIndex; i < max + maxIndex; i++) { //ทำการวนลูปแสดงค่าตัวเลขจำนวนเต็มที่อยู่ใน SubArray ที่ยาวที่สุด
            System.out.print(arr[i] + " ");//แสดงผลค่าตัวเลขที่อยู่ใน SubArray ที่ยาวที่สุด
        }
    }
}
